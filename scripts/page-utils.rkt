#lang racket

(require pict racket/draw) ;; already available with pict-utils?
;; (only-in slideshow para)
;; (only-in slideshow/text with-size with-style)
(require "pict-utils.rkt") ;; for mm->px px->mm etc

(provide (all-defined-out))

(define kind-of-drawing-context post-script-dc%)
(define post-script-setup (current-ps-setup))
(send post-script-setup set-paper-name "A4 210 x 297 mm")
(send post-script-setup set-margin 0 0);;
;; send dc get-size .. how to use boxes etc...
;; same as ps in pict-utils
;; (send post-script-setup set-scaling 1 1)
(define-values (w-edtr-mrgn h-edtr-mrgn)
  (values (box 0)(box 0)))
(send post-script-setup set-editor-margin 0 0)
;; (send post-script-setup get-editor-margin w-edtr-mrgn h-edtr-mrgn)

;; ; flows off page by a larg amount without default 0.8 scaling
;; (send post-script-setup set-scaling 1 1)

;; w-edtr-mrgn h-edtr-mrgn

#;(define-values (w-page-margin h-page-margin)
  (values (box 0) (box 0)))
;; (send post-script-setup get-margin w-page-margin h-page-margin)

;; subvocalize "-" as "of" or "for" or "in" depending on context?
;; ; need a "-" of and "_" "for" differentiation later??
;; ; Width of Page in MilliMeters
;; ; ; Must be same as Set-paper-name dimension above, A4 210 297

(define-values (w-page-mm h-page-mm)
  (values 210 297))
;; w-page-mm h-page-mm ; ok!
(match-define (list w-page h-page);; pixels implict for pict work
	      (map mm->px   ;; height OF page in mMeters
		   (list w-page-mm h-page-mm)))
;; w-page h-page ; ok! 594.300..1 840.51

;; Later make a transparent color 
(define (make-rect-layout w h (b-w 0) (b-c "white")) ;; rectangle for layout
  (rectangle w h #:border-width b-w #:border-color b-c))
(define rect-page ; rectangle of page size
  (make-rect-layout w-page h-page))
#; (define (get-dims pict) ; simper version in pict-utils, use map px->mm 
  (list "w:" (pict-width pict) "," "h:" (pict-height pict)))
(define (make-row-cells cell-pict (num-cells 2)) ;; row of laundered cells
  (apply hc-append (map launder (make-list num-cells cell-pict))))

;; page-margin start from A-one 72265 label sheet
(define-values (w-mrgn-tst h-mrgn-tst) (values (mm->px 4.75)
					       (mm->px 10.92))) ;; for testing
(define (make-page-pict pct w-margin h-margin)
  (pin-over rect-page w-margin h-margin pct))
;; test with 20 20

#;(define ps-dc (new kind-of-drawing-context
                  [interactive #t]
                  [use-paper-bbox #f]
                  [output "size-test.ps"])
  )
  
(define (place-pict-on-page pct w-margin h-margin page-file)
  (define pge-pct (make-page-pict pct w-margin h-margin))
  (define dc (new kind-of-drawing-context
		  [interactive #f];; #t when want to check ps setup through dialoge box
		  [use-paper-bbox #f]
		  [output page-file]))
  (send dc start-doc "starting page for printing")
  (send dc start-page)
  ;; Put pict on drawing context with proper starting point..
  (draw-pict pge-pct dc 0 0)
  (send dc end-page)
  (send dc end-doc)
  )

;; A-one 72265 38.1x21.2 start 4.75x10.92 row-sep 0 col-sep 2.5mm
;; need to make small, let ps scale to 0.8 and it will work??
(define test-rectangle (rectangle (mm->px 36.5) (mm->px 20) #:border-width 0))
;; (define test-rects (hc-append (mm->px 2) test-rectangle test-rectangle))
(define test-row (apply hc-append (mm->px 2) (make-list 5 test-rectangle)))
(define test-pict (apply vl-append (make-list 13 test-row))) ;; ok when printed on paper, not A4 sized when seen in evince??
 (define page-pict (make-page-pict test-pict w-mrgn-tst h-mrgn-tst))
;;  (place-pict-on-page test-pict w-mrgn-tst h-mrgn-tst "test-pict-on-page.ps") ; ok!

