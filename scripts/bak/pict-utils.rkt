#lang racket

(require pict pict-abbrevs racket/draw 
        (only-in slideshow para)
        (only-in slideshow/text with-size with-style)
	 )

(provide (all-defined-out))
(define font-style "Manjari")
(define font-size-bmp 42)
;; (define font-size-bmp 64)
;; (define font-size-A4 12)....
;; Might need a white background layer for images later
(define bgclr "white")
;; watermark settings just in case they are needed
(define wmrk-rtio (/ 1 15))
(define wmrk-txt-clr "gray")

;; page-ratios section to ----> page-ratios ok!
(define page-section-ratio 1/10);; Two 5-pic sequences in one column 
(define-values (page-w-rtio page-h-rtio)
  (values 1/5 1/10))
(define-values (paper-width-mm paper-height-mm) (values 210 297))
(define-values (slide-width-px slide-height-px) (values 1024 768))
;; convert millimeters to px for a common setup
(define pix-per-mm 2.83)
(define (mm->px d) (* pix-per-mm d));for dimension
(define-values (paper-width-px paper-height-px)
  (values (mm->px paper-width-mm) (mm->px paper-height-mm)))
                                                 
(define-values (paper-w-px paper-h-px)
  (values (mm->px paper-width-mm) (mm->px paper-height-mm)))
(define-values (slide-w-px slide-h-px)
  (values slide-width-px slide-height-px))
(define-values (w-szd h-szd)
  (values (* page-w-rtio paper-w-px) (* page-h-rtio paper-h-px)))
;; (filled-rectangle w-szd h-szd) ; page-ratios ok!

(define (scale-to-fit-txt-w-h txt wdt hgt
                              (cur-fnt-sze 12)(crnt-stle font-style))
			      ;; (mnm-rto .7)) ; (stle font-style)
  (define (size-para s w t)
    (with-size s (with-style crnt-stle(para #:width w t))))
    (define (help s)
      (define cur-try (size-para s wdt txt))
      (define cur-hgt (pict-height cur-try))
      ;; (define min-hgt (* mnm-rto cur-hgt))
      (cond
       ;; [(and (> hgt cur-hgt)(< min-hgt cur-hgt)) (help (add1 s))]
       ;; ; need a failsafe width adjustment went font-sizes won't
       ;; ; ; won't fit between the 
       [(> hgt cur-hgt) cur-try]
       [#t (help (sub1 s))]))
    (help cur-fnt-sze))

(define (make-wmrk-pict txt pct)
  (colorize (scale-to-fit-txt-w-h (text txt)
				  (* wmrk-rtio (pict-width pct))
				  (* wmrk-rtio (pict-height pct)))
	    wmrk-txt-clr))
(define (add-wmrk txt pct)
  (lb-superimpose pct
		  (make-wmrk-pict txt pct)))

(define (make-bgd-pct w h)
  (filled-rectangle w h #:color bgclr #:draw-border? #f))
(define (add-bgd-lyr pct)
  (define-values (w h)
    (values (pict-width pct) (pict-height pct)))
  (define bgd-lyr (make-bgd-pct w h))
  (cc-superimpose bgd-lyr pct))
(define (add-wmk-lyr pct txt txt-clr sze-rto (impse lb-superimpose))
  (define-values (w h)
    (values (pict-width pct) (pict-height pct)))  
  (impse pct (scale-to-fit (add-bgd-lyr (colorize (text txt) txt-clr))
	 (* sze-rto w) (* sze-rto h))))

(define (write-pict->svg pct pth) ;; needs racket/draw
  ;; pict , relative(or full) filepath
  (define-values (w h)
    (values (pict-width pct) (pict-height pct)))
  (define dc (new svg-dc% [width w] [height h]
		  [output pth] [exists 'replace]))
  (send dc start-doc "start")
  (send dc start-page)
  (draw-pict pct dc 0 0)
  (send dc end-page)(send dc end-doc))

(define (save-pct-to-png pct pth)
  (save-pict pth pct 'png))
(define (save-pct-to-jpg pct pth)
  (save-pict pth pct 'jpeg))
