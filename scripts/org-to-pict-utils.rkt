#lang racket

(require pict (only-in slideshow para)
        slideshow/text)
(require (for-syntax pict))
(provide all-defined-out)

(define text-basic-style 'default) ; "Manjari")
(define text-basic-size 12)
(define smpl-orgm " /neologisms/, such as *\"Big Brother\"*, *\"Thought Police\"*, \"Two Minutes Hate\", \"Room 101\", *\"memory hole\"*, \"Newspeak\", *\"doublethink\"*, \"proles\", \"unperson\", and \"thoughtcrime\".")

 (define (org->pict-italic str) ; bold
  (regexp-replace* #rx"/(.*?)/" str "(text \"\\1\" (cons 'italic basic-style))"))

(define-syntax-rule (org->pict-italic-sntx str)
  (text 
   (regexp-replace* #rx"/(.*?)/" str "\"\\1\"")
   (cons 'bold basic-style)))
(define (org->pict-bold str) ; bold
  (regexp-replace* #rx"\\*(.*?)\\*" str "text \"\\1} (cons 'bold basic-style"))

;; (org->pict-italic-sntx smpl-orgm)

#; (with-size text-basic-size ; ok with various requires...
(para #:width 200 (text "neologisms" (cons 'italic text-basic-style)) ", such as"
     (text "Big Brother" (cons 'bold text-basic-style)) "," (text "Thought Police"
                                                                 (cons 'bold text-basic-style)))
  )

(define-syntax (to-para-W-syntx stx)
  (syntax-case stx ()
    ((_to-para qtd-txt-N-pics)
     #'(para qtd-txt-N-pics))
    ((_to-para qtd-txt-N-pics int)
     #'(para #:width int qtd-txt-N-pics))))
(to-para-W-syntx "This is some text for a paragraph") ; ok!
(to-para-W-syntx "This is some text for a paragraph" 200) ; ok!

