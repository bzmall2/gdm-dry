#lang racket

(require "../gdm-dry-utils.rkt" "../pict-utils.rkt" "../page-utils.rkt")

(require "../org-to-pict-utils.rkt")

;; (define in-file "./in.org")
;; (define out-file "print.ps")
(define in-file "../../essays/Extra-Prints/Orwell-Roy-Russel-Hedges-Huxley/in.org")
(define out-file "../../essays/Extra-Prints/Orwell-Roy-Russel-Hedges-Huxely/print.ps")

(define in-string (file->string in-file))
;; in-string ; ok!

(define in-sects (string-split in-string "**"))
;; in-sects ; ok!
(define sects-list (rest in-sects))
;; (last in-sects) ; ok!

(define (sect->parts sect)
  (string-split sect #px"\n\n+"))
;; (sect->parts (first sects-list)) ; ok!

