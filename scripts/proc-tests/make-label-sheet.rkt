#lang racket

(require pict)

(define lbl-txt "labels/label-now.txt")
(define sep "：") ;; could be ":" or "," or "|"...


(define lines (file->lines lbl-txt))
;;  lines ; ok!

(define (line-split line)
  (string-split line sep))

(define line->row-list
  (lambda (line)
    (add-between
     (line-split line)
     sep)))

(define row-lists
  (map line->row-list lines))
;; row-lists ; ok!


(define lbl-tbl
  (table 3
	 (map (lambda (x)
		(text x))
	      (flatten row-lists))
	 lc-superimpose
	 lc-superimpose
	 1 1))

(define (mm->px x)
  (* x 3.779528))
(define (px->mm x)
  (/ x 3.779258))
(define sect-w (mm->px 38.1))
(define sect-h (mm->px 21.2))
(define sect-cnvs (blank sect-w sect-h))
(define sect-rect (rectangle sect-w sect-h #:border-color "light gray" #:border-width 1))
(define sect-labl (scale-to-fit lbl-tbl sect-w sect-h))
(define sect-prnt (cc-superimpose sect-labl sect-cnvs))

(define ink-part
  (table 5
        (make-list 65 sect-prnt)
        cc-superimpose
        cc-superimpose
        (mm->px 2.5)
        0))
ink-part
(px->mm (pict-width ink-part))
(px->mm (pict-height ink-part))


