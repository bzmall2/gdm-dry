#! /usr/bin/env racket
#lang racket
(require simple-qr pict)
(require "gdm-dry-utils.rkt" "pict-utils.rkt");; "pict-utils.rkt")

(define print-page-file "print-page.ps")
(define-values (default-lesson-dir default-act-dir-dir default-psn-etc-dir default-img-dir)
  ;;(values "qr-codes" "online-presence" "BS2-fsa-SNS-print-page" "pngs"))
  (values "lessons" "ETP-3" "pages-1-34" "pngs"))
(define lines-file "3-lines-for-printing-w-qrs.txt")
;; set a starting font-size, will be decreased if text
;; ; doesn't fit into text-area later
(define default-font-size 12) ;; 22
;; work in a maximum size for group, avoid huge picts
(define minimal-page-grouping-num 10)
(define (set-group-pict-height num)
  (/ 2 (+ 1 (* 2 num)))) ;; if 10 groups 2/21 or page is the height

;; are there arguments from the command line to adjust DrRacekt defaults?
(define clia-v (current-command-line-arguments)) ;; arguments come in as vector '#()
(define cli? (< 0 (vector-length clia-v))) ;; no arguments if the vector is empty
(define lesson-directory
  (if cli? (current-directory-for-user) default-lesson-dir)) ;; qr-codes
(define-values (act-dir psn-tng-plc tf) ;; tf handles extra value from split-path
  (if cli? (split-path (vector-ref clia-v 0))
     (values default-act-dir-dir default-psn-etc-dir #f)))
(define pngs-diry-name "pngs") ;; later imgs, adapt to svgs, jpgs, etc...

;; Adjust page-size-settings; leave 1/11 of space for side margins
(define group-pict-width (* 10/11 paper-width-px))
;; add one to double of groups, then take 2 parts for the height
;; ; leave some blank leeway margin at the bottom of the page
(define (make-group-height-ratio groups-list)
  (define lng-grp-lst (length groups-list))
  (if (> 10 lng-grp-lst) (set-group-pict-height minimal-page-grouping-num)
     (set-group-pict-height lng-grp-lst))) ;; if 10 groups 2/21 ratio

;;;; Set up directories
(define lesson-path  ;; need a more elegant solution to replace this hack
  (if cli? (build-path lesson-directory act-dir psn-tng-plc)
     (build-lesson-path lesson-directory
                       act-dir psn-tng-plc)))
(define print-page-path (build-path lesson-path
                                   print-page-file))

(define pngs-diry-path (build-sectn-path lesson-path pngs-diry-name))
(void  ;; avoid notification about exit status #f for whatever
 (map make-diry-if-needed (list pngs-diry-path )))
;;;;;

(define lines-string
  (string-trim (file->string (build-path lesson-path lines-file))))
;; (list pngs-diry-path lines-string) ; ok!

;;;;; 
(define (get-group-strings string-from-file)
  (remove-blank-lines
   (map string-trim (string-split string-from-file group-sep))))

(define group-strings ;; list of strings
  (get-group-strings lines-string))
;; group-strings ; ok!
(define (group-string->group-records group-string)
  (if (regexp-match (regexp (regexp-quote record-sep)) group-string)
      (map string-trim (string-split group-string record-sep))
      (map string-trim (string-split group-string (regexp"\n")))))    ; else

(define groups
  (map group-string->group-records group-strings))
;; use group number (length groups-list) to set group-pict height
(define group-pict-height (* (make-group-height-ratio groups) paper-height-px))
;;;; write-pngs in preparation to read into pict as bitmap
(define (write-qr-pngs group)
  (define png-name (second group))
  (define qr-data (third group))
  (define qr-path (build-path pngs-diry-path png-name))
  ;;(write-qr-sys qr-data qr-path)
  (qr-write qr-data qr-path)
  )

(for-each write-qr-pngs groups)

(define (group->pict-w-qr group)
  (match-define (list text png-name qr-data) group)
  (define-values (text-pict-w text-pict-h)
    (values  (- group-pict-width group-pict-height)
            group-pict-height))
  (define qr-path (build-path pngs-diry-path png-name))
  (define qr-pict (scale-to-fit (bitmap qr-path) #:mode 'preserve
                               group-pict-height group-pict-height ))
  (define (get-text-list grp)
    (string-split (first grp) #px"\n+"))
  (define text-list (get-text-list group))
  
  (define (pict-from-multi-lines lne-lst)
    (define lne-hgt (* (/ 1 (length lne-lst)) text-pict-h))
    (define (make-line-pict txt)
      (scale-to-fit-txt-w-h txt text-pict-w lne-hgt 14));default-font-size
    (apply vl-append (map make-line-pict lne-lst)))
  
  (define text-pict (if (< 1 (length text-list))
                       (pict-from-multi-lines text-list)
                       (scale-to-fit-txt-w-h (first text-list)
                                            text-pict-w text-pict-h default-font-size)))
  (ht-append
   qr-pict text-pict))

;; (map group->pict-w-qr groups)

(define pict-for-print (apply vl-append
                              (map group->pict-w-qr groups)))

(pict->pspage pict-for-print print-page-path)
