#+TITLE: Jeff Schimdt Disciplined Minds
#+STARTUP: showall
#+OPTIONS: toc:nil num:nil timestamp:nil
#+HTML_HEAD: <style> @page { size: 210mm 297mm; margin: 4mm; margin-bottom: 6mm; } </style>
#+HTML_HEAD: <style> h1.title { display: none; }  </style>
#+HTML_HEAD: <style> h1, h2, h3, h4 { font-weight: 500; font-size: 1.2em; }  </style>
#+HTML_HEAD: <style> div#postamble { display: none; }  </style>

** 145-146
#+BEGIN_QUOTE
... the assistant professor must get as much published as possible, and so he pursues the work that he knows best: topics closely related to his dissertation. And he does this work in the way most likely to meet the approval of journal referees, namely, conservatively, being careful not to raise any controversy.

  Whether the young professional has surrendered political control through the normal process of qualification or by somehow slipping through the qualification system but then failing to conspire with like-minded people, he turns his efforts toward achieving along more traditional lines. Thus the yuppie finds himself running in the conventional rat race. He follow the course of this race obediently and aggressively, with all his energy. He measures his progress /quantitatively/ now, in tender valued through the system: bucks pulled down, possessions accumulate, position attained. Scientists are no less likely than other professionals to follow this course. The university scientist gets recognition, status, promotions and and raises by publishing papers. The industrial scientist keeps score by publishing papers and also by getting patents. Thus, the scientist whose bottom line is defined by the market is likely to view a research project in terms of the papers or patents that can be mined from it. There must be many such scientists, as scientific journals are filled with papers on narrow topics of little interest that have obviously been published more to advance careers than to advance knowledge.

Nothing reveals more clearly the degree to which employed professionals are alienated from their subjects that does the sharply contrasting behavior of the hobbyists or "buffs" in their fields. When hobbyists encounter one another at a social gathering, before long you will find them talking eagerly about the content of their subject of common interest, showing excitement, enthusiasm, wonder and curiosity that is reminiscent of beginning professional students. This rarely happens when professionals talk casually with their colleagues. Unlike the amateurs, the professionals don't talk much about the work itself; they often appear detached from their subject, as if they don't derive much satisfaction from it. Yes, the "talk shop," but their focus is so far from the content of the work itself that you would have a hard time if you had to guess what kind of "shop" they work in. A commercial bank? A junior high school? A government agency? A university department? Casual conversation among professionals tends to focus on the actions and personalities of employers and powerful figures within their fields---the standard topics of the powerless. Their gossip is by no means idle, however, for the politics are central to their work as professionals.

# The... 
...  at a wine-and-cheese reception after an English department colloquium, a first-year graduate student musters the courage to approach the speaker, a well-known professor from another university, and ask a question about literature. But before the conversation has gotten very far, a local faculty member walks up and derails it with the question /he/ has been waiting to ask: "Is Jones really planning to leave Yale? I heard a rumor." Soon the tow professors are engrossed in a wide-ranging discussion about job openings around the country, research grants, book contracts, journal editors and who's jockeying for power in the field. the graduate student, realizing that the conversation is not going to return to the evidently less important topic of literature, retreats back into the crowd. Versions of this generic scene occur frequently in every field.

The professors here symbolize the tragedy of all employed professionals who started out as students loving their subjects. Such students submit themselves to the process of professional training in an effort to be free of the marketplace, but instead of being strengthened by the process they are crippled by it. Deprived of political control over their  own work, the become alienated from their subjects and measure their lives in the marketplace.

  --- Jeff Schimdt in *Disciplined Minds* pp. 145-146
#+END_QUOTE

#+BEGIN_QUOTE
Oh it's a disgrace
to see the human race
in a Rat Race

 --- Bob Marley in "Rat Race"
#+END_QUOTE

  #JeffSchimdt #facdev #FD #facultydevelopment 

[[file:imgs/JeffSchmidt-DisciplinedMinds-p145-1.jpg]]
[[file:imgs/JeffSchmidt-DisciplinedMinds-p145-2.jpg]]
[[file:imgs/JeffSchmidt-DisciplinedMinds-p146-1.jpg]]
