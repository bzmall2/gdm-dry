(set-registry ?A "#+ATTR_HTML: ")
(fset 'org-html-attribute
   (lambda (&optional arg) "Keyboard macro." (interactive "p") (kmacro-exec-ring-item (quote ("riA" 0 "%d")) arg)))
(global-set-key [24 11 65] 'org-html-attribute)
					; C-x-CkA

(set-register ?N "#+name: ")
(fset 'org-block-name-insert
   (lambda (&optional arg) "Keyboard macro." (interactive "p") (kmacro-exec-ring-item (quote ("riN" 0 "%d")) arg)))
(global-set-key [24 11 84] 'org-block-name-insert)
; C-xC-kT


(set-register ?C "#+CAPTION: ")
(fset 'org-html-table-caption
   (lambda (&optional arg) "Keyboard macro." (interactive "p") (kmacro-exec-ring-item (quote ("riC" 0 "%d")) arg)))
(global-set-key [24 11 67] 'org-html-table-caption)
; C-xC-kC
