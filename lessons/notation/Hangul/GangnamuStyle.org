#+TITLE: 한글 
#+STARTUP: showall
#+OPTIONS: toc:nil num:nil timestamp:nil author:nil
#+HTML_HEAD: <style> h2, h3 { font-weight: 500; font-size: 11pt; text-indent: 3em; }  </style>
#+HTML_HEAD: <style> h1.title {display: none; }  </style>
# #+HTML_HEAD: <style> p { font-size: 3em; }  </style>

#+HTML_HEAD: <style> span.table-number { display: none; }  </style>
#+HTML_HEAD: <style> td.left { padding: .1em 2em .1em .1em; }  </style>

#+HTML_HEAD: <style> a { text-decoration: none; color: black; }  </style>


** Lesson 1: Vowels

#+CAPTION: Basic Vowels
| 1  | 2  | 3  | 4   | 5  | 6  | 7  | 8  | 9  | 10 |
| dk | di | dj | du  | dh | dy | dn | db | dm | dl |
| 아 | 야 | 어 | 여  | 오 | 요 | 우 | 유 | 으 | 이 |
| a  | ya | u1 | yu1 | o  | yo | u  | yu | w  | i  |

#+CAPTION: Doubled Vowels
| 1 | 2  | 3  |  4 |  5 | 6   | 7   |   8 |  9 | 10 | 11 |
|   | do | dp |    |    | dho | hdl |     |    |    |    |
|   | 애 | 에 |    |    | 왜  | 외  |     |    |    |    |
| E | yE | e  | ye | wa | wE  | oi  | uu1 | ue | ui | wi |


** korean-hangul key sequences
set-input-method korean-hangul
# *** KEY SEQUENCE
You can input characters by the following key sequences:
key char  [type a key sequence to insert the corresponding character]

| D   ㆁ | e   ㄷ | s   ㄴ | _d  ㆆ | fv ㄿ  | qt     ㅄ | uk  ㆍ | qte     ㅵ |
| E   ㄸ | f   ㄹ | t   ㅅ | aD  ㅱ | fx ㄾ  | qw     ㅶ | vD  ㆄ | qtr     ㅴ |
| G   ㆅ | g   ㅎ | u   ㅕ | am  ㏂ | hk ㅘ  | qx     ㅷ | wn  ㈜ | st_     ㅨ |
| O   ㅒ | h   ㅗ | v   ㅍ | aq  ㅮ | hl ㅚ  | rt     ㄳ | yO  ㆈ | ukl     ㆎ |
| P   ㅖ | i   ㅑ | w   ㅈ | at  ㅯ | ho ㅙ  | se     ㅦ | yi  ㆇ | won     ￦ |
| Q   ㅃ | j   ㅓ | x   ㅌ | bP  ㆋ | ks ㉿  | sg     ㄶ | yl  ㆉ | yen     ￥ |
| R   ㄲ | k   ㅏ | y   ㅛ | bl  ㆌ | ml ㅢ  | st     ㅧ | Dt_ ㆃ | ddag    ‡ |
| S   ㅥ | l   ㅣ | z   ㅋ | bu  ㆊ | nj ㅝ  | sw     ㄵ | Tel ℡ | pound   ￡ |
| T   ㅆ | m   ㅡ | Co  ㏇ | fa  ㄻ | nl ㅟ  | t_     ㅿ | at_ ㅰ | percent ‰ |
| W   ㅉ | n   ㅜ | DD  ㆀ | fe  ㅪ | np ㅞ  | te     ㅼ | dag † |            |
| a   ㅁ | o   ㅐ | Dt  ㆂ | fg  ㅀ | pm ㏘  | tq     ㅽ | f_d ㅭ |            |
| b   ㅠ | p   ㅔ | No  № | fq  ㄼ | qD ㅸ  | tr     ㅺ | fqt ㅫ |            |
| c   ㅊ | q   ㅂ | QD  ㅹ | fr  ㄺ | qe ㅳ  | ts     ㅻ | frt ㅩ |            |
| d   ㅇ | r   ㄱ | TM  ™  | ft  ㄽ | qr  ㅲ | tw     ㅾ | ft_ ㅬ |            |

*** example

 - g - ㅎ k - ㅏ s - ㄴ r - ㄱ (this completes the first character and starts the next)
 - m - ㅡ f - ㄹ \C-space - to finish inputing the last character
   - 한글   dh오 dm으

**** source
https://www.emacswiki.org/emacs/WritingKorean

** image source
http://www.emagasia.com/korean-keyboard
 - links to
   - Korean verbs: http://www.emagasia.com/korean-verbs
   - English Comparison: http://www.emagasia.com/comparison-of-korean-and-english

** Fix font
  - sources
    - https://eatpeppershothot.blogspot.jp/2014/05/configure-emacs-to-use-truetype-korean.html
      - http://wiki.kldp.org/wiki.php/EmacsChangeFonts#s-5.2

#+BEGIN_SRC emacs-lisp
(defun xftp (&optional frame)
  "Return t if FRAME support XFT font backend."
  (let ((xft-supported))
    (mapc (lambda (x) (if (eq x 'xft) (setq xft-supported t)))
          (frame-parameter frame 'font-backend))
    xft-supported))

(when (xftp)
  (let ((fontset "fontset-default"))
    (set-fontset-font fontset 'latin
                      '("DejaVu Sans Mono" . "unicode-bmp"))
    (set-fontset-font fontset 'hangul
                      '("NanumGothic" . "unicode-bmp"))
    (set-face-attribute 'default nil
                        :font fontset
:height 110)))
#+END_SRC

** learn korean
http://www.learnkoreanlanguage.com/learn-hangul.html

** 한글 심벌 입력표

한글 심벌 입력표


** 강남스타일 나사

오빤 강남스타일
강남스타일

낮에는 따사로운 인간적인 여자
커피 한잔의 여유를 아는 품격 있는 여자
밤이 오면 심장이 뜨거워지는 여자
그런 반전 있는 여자

나는 사나이
낮에는 너만큼 따사로운 그런 사나이
커피 식기도 전에 원샷 때리는 사나이
밤이 오면 심장이 터져버리는 사나이
그런 사나이

아름다워 사랑스러워
그래 너 hey 그래 바로 너 hey

아름다워 사랑스러워
그래 너 hey 그래 바로 너 hey
지금부터 갈 데까지 가볼까

오빤 강남스타일
강남스타일

오빤 강남스타일
강남스타일

오빤 강남스타일

Eh- Sexy Lady
오빤 강남스타일

Eh- Sexy Lady
에에에에에에

정숙해 보이지만 놀 땐 노는 여자
이때다 싶으면 묶었던 머리 푸는 여자
가렸지만 웬만한 노출보다 야한 여자
그런 감각적인 여자

나는 사나이
점잖아 보이지만 놀 땐 노는 사나이
때가 되면 완전 미쳐버리는 사나이
근육보다 사상이 울퉁불퉁한 사나이
그런 사나이

아름다워 사랑스러워
그래 너 hey 그래 바로 너 hey

아름다워 사랑스러워
그래 너 hey 그래 바로 너 hey
지금부터 갈 데까지 가볼까

오빤 강남스타일
강남스타일

오빤 강남스타일
강남스타일

오빤 강남스타일

Eh- Sexy Lady
오빤 강남스타일

Eh- Sexy Lady
에에에에에에

뛰는 놈 그 위에 나는 놈
baby baby
나는 뭘 좀 아는 놈

뛰는 놈 그 위에 나는 놈
baby baby
나는 뭘 좀 아는 놈

You know what I’m saying

오빤 강남스타일

Eh- Sexy Lady
오빤 강남스타일

Eh- Sexy Lady
오빤 강남스타일

Read more at http://www.lyrics.com/gangnam-style-lyrics-psy-pm-832.html#XzWeBR2o159Dwffq.99

*** Translation
Oppa is Kangnam style                                                   
Kangnam style

A girl who is warm and humanle during the day
A classy girl who know how to enjoy the freedom of a cup of coffee
A girl whose heart gets hotter when night comes
A girl with that kind of twist

I’m a guy
A guy who is as warm as you during the day
A guy who one-shots his coffee before it even cools down
A guy whose heart bursts when night comes
That kind of guy

Beautiful, loveable
Yes you, hey, yes you, hey
Beautiful, loveable
Yes you, hey, yes you, hey
Now let’s go until the end

Oppa is Kangnam style, Kangnam style
Oppa is Kangnam style, Kangnam style
Oppa is Kangnam style

Eh- Sexy Lady, Oppa is Kangnam style
Eh- Sexy Lady oh oh oh oh

A girl who looks quiet but plays when she plays
A girl who puts her hair down when the right time comes
A girl who covers herself but is more sexy than a girl who bares it all
A sensable girl like that

I’m a guy
A guy who seems calm but plays when he plays
A guy who goes completely crazy when the right time comes
A guy who has bulging ideas rather than muscles
That kind of guy

Beautiful, loveable
Yes you, hey, yes you, hey
Beautiful, loveable
Yes you, hey, yes you, hey
Now let’s go until the end

Oppa is Kangnam style, Kangnam style
Oppa is Kangnam style, Kangnam style
Oppa is Kangnam style

Eh- Sexy Lady, Oppa is Kangnam style
Eh- Sexy Lady oh oh oh oh

On top of the running man is the flying man, baby baby
I’m a man who knows a thing or two
On top of the running man is the flying man, baby baby
I’m a man who knows a thing or two

You know what I’m saying
Oppa is Kangnam style

Eh- Sexy Lady, Oppa is Kangnam style
Eh- Sexy Lady oh oh oh oh
Oppa is Gangnam Style

Read more at http://www.lyrics.com/gangnam-style-lyrics-psy-pm-832.html#XzWeBR2o159Dwffq.99

*** Transliteration
Oppan Gangnam Style
Gangnam Style

Najeneun ddasarowun in-ganjeogin yeoja
Keopi hanjaneui yeoyureul aneun pumgyeok ittneun yeoja
Bami omyeon shimjangi ddeugeowojineun yeoja
Geureon banjeon ittneun yeoja

Naneun sanai
Najeneun neomankeum ddasarowun geureon sanai
Keopi shik-gido jeone One Shot ddaerineun sanai
Bami omyeon shimjangi teojyeobeorineun sanai
Geureon sanai

Aremdawo sarangseurowo
Geurae neo hey, geurae baro neo hey

Areumdawo sarangseurowo
Geurae neo hey, geurae baro neo hey
Jigeumbuteo gal ddaekkaji gabolkka

Oppan Gangnam style
Gangnam style

Oppan Gangnam style
Gangnam style

Oppan Gangnam style

Eh~ Sexy Lady
Oppan Gangnam style

Eh~ Sexy Lady
Eh, eh, eh, eh, eh, eh

Jeongsokhae bo-ijiman nol ddaen noneun yeoja
Iddaeda shipeumyeon mukkeottdeon meori puneun yeoja
Garyeottjiman wenmanhan nochulboda yahan yeoja
Geureon gangjakjeogin yeoja

Naneun sanai
Jeonjanha bo-ijiman nol ddaen noneun sanai
Ddaega dweimyeon wanjeon michyeobeorineun sanai
Geun-yukboda sasangi ultungbultung han sanai
Geureon sanai

Aremdawo sarangseurowo
Geurae neo hey, geurae baro neo hey

Areumdawo sarangseurowo
Geurae neo hey, geurae baro neo hey
Jigeumbuteo gal ddaekkaji gabolkka

Oppan Gangnam style
Gangnam style

Oppan Gangnam style
Gangnam style

Oppan Gangnam style

Eh~ Sexy Lady
Oppan Gangnam style

Eh~ Sexy Lady
Eh, eh, eh, eh, eh, eh

Ttwineun nom geu wi-e naneun nom
baby baby
Naneun mwol jom aneun nom

Ttwineun nom geu wi-e naneun nom
baby baby
Naneun mwol jom aneun nom

You know what I’m saying

Oppan Gangnam style

Eh~ Sexy Lady
Oppan Gangnam style

Eh~ Sexy Lady
Oppan Gangnam style                                                        

Read more at http://www.lyrics.com/gangnam-style-lyrics-psy-pm-832.html#XzWeBR2o159Dwffq.99

**** Gangname Style Table

| 오빤 강남스타일                        | Oppan Gangnam Style                                    | Oppan Gangnam Style                                    |
| 강남스타일                             | Gangnam Style                                          | Gangnam Style                                          |
|                                        |                                                        |                                                        |
| 낮에는 따사로운 인간적인 여자          | Najeneun ddasarowun in-ganjeogin yeoja                 | Najeneun ddasarowun in-ganjeogin yeoja                 |
| 커피 한잔의 여유를 아는 품격 있는 여자 | Keopi hanjaneui yeoyureul aneun pumgyeok ittneun yeoja | Keopi hanjaneui yeoyureul aneun pumgyeok ittneun yeoja |
| 밤이 오면 심장이 뜨거워지는 여자       | Bami omyeon shimjangi ddeugeowojineun yeoja            | Bami omyeon shimjangi ddeugeowojineun yeoja            |
| 그런 반전 있는 여자                    | Geureon banjeon ittneun yeoja                          | Geureon banjeon ittneun yeoja                          |
|                                        |                                                        |                                                        |
| 나는 사나이                            | Naneun sanai                                           | Naneun sanai                                           |
| 낮에는 너만큼 따사로운 그런 사나이     | Najeneun neomankeum ddasarowun geureon sanai           | Najeneun neomankeum ddasarowun geureon sanai           |
| 커피 식기도 전에 원샷 때리는 사나이    | Keopi shik-gido jeone One Shot ddaerineun sanai        | Keopi shik-gido jeone One Shot ddaerineun sanai        |
| 밤이 오면 심장이 터져버리는 사나이     | Bami omyeon shimjangi teojyeobeorineun sanai           | Bami omyeon shimjangi teojyeobeorineun sanai           |
| 그런 사나이                            | Geureon sanai                                          | Geureon sanai                                          |
|                                        |                                                        |                                                        |
| 아름다워 사랑스러워                    | Aremdawo sarangseurowo                                 | Aremdawo sarangseurowo                                 |
| 그래 너 hey 그래 바로 너 hey           | Geurae neo hey, geurae baro neo hey                    | Geurae neo hey, geurae baro neo hey                    |
|                                        |                                                        |                                                        |
| 아름다워 사랑스러워                    | Areumdawo sarangseurowo                                | Areumdawo sarangseurowo                                |
| 그래 너 hey 그래 바로 너 hey           | Geurae neo hey, geurae baro neo hey                    | Geurae neo hey, geurae baro neo hey                    |
| 지금부터 갈 데까지 가볼까              | Jigeumbuteo gal ddaekkaji gabolkka                     | Jigeumbuteo gal ddaekkaji gabolkka                     |
|                                        |                                                        |                                                        |
| 오빤 강남스타일                        | Oppan Gangnam style                                    | Oppan Gangnam style                                    |
| 강남스타일                             | Gangnam style                                          | Gangnam style                                          |
|                                        |                                                        |                                                        |
| 오빤 강남스타일                        | Oppan Gangnam style                                    | Oppan Gangnam style                                    |
| 강남스타일                             | Gangnam style                                          | Gangnam style                                          |
|                                        |                                                        |                                                        |
| 오빤 강남스타일                        | Oppan Gangnam style                                    | Oppan Gangnam style                                    |
|                                        |                                                        |                                                        |
| Eh- Sexy Lady                          | Eh~ Sexy Lady                                          | Eh~ Sexy Lady                                          |
| 오빤 강남스타일                        | Oppan Gangnam style                                    | Oppan Gangnam style                                    |
|                                        |                                                        |                                                        |
| Eh- Sexy Lady                          | Eh~ Sexy Lady                                          | Eh~ Sexy Lady                                          |
| 에에에에에에                           | Eh, eh, eh, eh, eh, eh                                 | Eh, eh, eh, eh, eh, eh                                 |
|                                        |                                                        |                                                        |
| 정숙해 보이지만 놀 땐 노는 여자        | Jeongsokhae bo-ijiman nol ddaen noneun yeoja           | Jeongsokhae bo-ijiman nol ddaen noneun yeoja           |
| 이때다 싶으면 묶었던 머리 푸는 여자    | Iddaeda shipeumyeon mukkeottdeon meori puneun yeoja    | Iddaeda shipeumyeon mukkeottdeon meori puneun yeoja    |
| 가렸지만 웬만한 노출보다 야한 여자     | Garyeottjiman wenmanhan nochulboda yahan yeoja         | Garyeottjiman wenmanhan nochulboda yahan yeoja         |
| 그런 감각적인 여자                     | Geureon gangjakjeogin yeoja                            | Geureon gangjakjeogin yeoja                            |
|                                        |                                                        |                                                        |
| 나는 사나이                            | Naneun sanai                                           | Naneun sanai                                           |
| 점잖아 보이지만 놀 땐 노는 사나이      | Jeonjanha bo-ijiman nol ddaen noneun sanai             | Jeonjanha bo-ijiman nol ddaen noneun sanai             |
| 때가 되면 완전 미쳐버리는 사나이       | Ddaega dweimyeon wanjeon michyeobeorineun sanai        | Ddaega dweimyeon wanjeon michyeobeorineun sanai        |
| 근육보다 사상이 울퉁불퉁한 사나이      | Geun-yukboda sasangi ultungbultung han sanai           | Geun-yukboda sasangi ultungbultung han sanai           |
| 그런 사나이                            | Geureon sanai                                          | Geureon sanai                                          |
|                                        |                                                        |                                                        |
| 아름다워 사랑스러워                    | Aremdawo sarangseurowo                                 | Aremdawo sarangseurowo                                 |
| 그래 너 hey 그래 바로 너 hey           | Geurae neo hey, geurae baro neo hey                    | Geurae neo hey, geurae baro neo hey                    |
|                                        |                                                        |                                                        |
| 아름다워 사랑스러워                    | Areumdawo sarangseurowo                                | Areumdawo sarangseurowo                                |
| 그래 너 hey 그래 바로 너 hey           | Geurae neo hey, geurae baro neo hey                    | Geurae neo hey, geurae baro neo hey                    |
| 지금부터 갈 데까지 가볼까              | Jigeumbuteo gal ddaekkaji gabolkka                     | Jigeumbuteo gal ddaekkaji gabolkka                     |
|                                        |                                                        |                                                        |
| 오빤 강남스타일                        | Oppan Gangnam style                                    | Oppan Gangnam style                                    |
| 강남스타일                             | Gangnam style                                          | Gangnam style                                          |
|                                        |                                                        |                                                        |
| 오빤 강남스타일                        | Oppan Gangnam style                                    | Oppan Gangnam style                                    |
| 강남스타일                             | Gangnam style                                          | Gangnam style                                          |
|                                        |                                                        |                                                        |
| 오빤 강남스타일                        | Oppan Gangnam style                                    | Oppan Gangnam style                                    |
|                                        |                                                        |                                                        |
| Eh- Sexy Lady                          | Eh~ Sexy Lady                                          | Eh~ Sexy Lady                                          |
| 오빤 강남스타일                        | Oppan Gangnam style                                    | Oppan Gangnam style                                    |
|                                        |                                                        |                                                        |
| Eh- Sexy Lady                          | Eh~ Sexy Lady                                          | Eh~ Sexy Lady                                          |
| 에에에에에에                           | Eh, eh, eh, eh, eh, eh                                 | Eh, eh, eh, eh, eh, eh                                 |
|                                        |                                                        |                                                        |
| 뛰는 놈 그 위에 나는 놈                | Ttwineun nom geu wi-e naneun nom                       | Ttwineun nom geu wi-e naneun nom                       |
| baby baby                              | baby baby                                              | baby baby                                              |
| 나는 뭘 좀 아는 놈                     | Naneun mwol jom aneun nom                              | Naneun mwol jom aneun nom                              |
|                                        |                                                        |                                                        |
| 뛰는 놈 그 위에 나는 놈                | Ttwineun nom geu wi-e naneun nom                       | Ttwineun nom geu wi-e naneun nom                       |
| baby baby                              | baby baby                                              | baby baby                                              |
| 나는 뭘 좀 아는 놈                     | Naneun mwol jom aneun nom                              | Naneun mwol jom aneun nom                              |
|                                        |                                                        |                                                        |
| You know what I’m saying              | You know what I’m saying                              | You know what I’m saying                              |
|                                        |                                                        |                                                        |
| 오빤 강남스타일                        | Oppan Gangnam style                                    | Oppan Gangnam style                                    |
|                                        |                                                        |                                                        |
| Eh- Sexy Lady                          | Eh~ Sexy Lady                                          | Eh~ Sexy Lady                                          |
| 오빤 강남스타일                        | Oppan Gangnam style                                    | Oppan Gangnam style                                    |
|                                        |                                                        |                                                        |
| Eh- Sexy Lady                          | Eh~ Sexy Lady                                          | Eh~ Sexy Lady                                          |
| 오빤 강남스타일                        | Oppan Gangnam style                                    | Oppan Gangnam style                                    |

***** K n E only
| 오빤 강남스타일                        | Oppa is Kangnam style                                            |
| 강남스타일                             | Kangnam style                                                    |
|                                        |                                                                  |
| 낮에는 따사로운 인간적인 여자          | A girl who is warm and humanle during the day                    |
| 커피 한잔의 여유를 아는 품격 있는 여자 | A classy girl who know how to enjoy the freedom of a cup of coff |
| 밤이 오면 심장이 뜨거워지는 여자       | A girl whose heart gets hotter when night comes                  |
| 그런 반전 있는 여자                    | A girl with that kind of twist                                   |
|                                        |                                                                  |
| 나는 사나이                            | I’m a guy                                                       |
| 낮에는 너만큼 따사로운 그런 사나이     | A guy who is as warm as you during the day                       |
| 커피 식기도 전에 원샷 때리는 사나이    | A guy who one-shots his coffee before it even cools down         |
| 밤이 오면 심장이 터져버리는 사나이     | A guy whose heart bursts when night comes                        |
| 그런 사나이                            | That kind of guy                                                 |
|                                        |                                                                  |
| 아름다워 사랑스러워                    | Beautiful, loveable                                              |
| 그래 너 hey 그래 바로 너 hey           | Yes you, hey, yes you, hey                                       |
|                                        |                                                                  |
| 아름다워 사랑스러워                    | Beautiful, loveable                                              |
| 그래 너 hey 그래 바로 너 hey           | Yes you, hey, yes you, hey                                       |
| 지금부터 갈 데까지 가볼까              | Now let’s go until the end                                      |
|                                        |                                                                  |
| 오빤 강남스타일                        | Oppa is Kangnam style, Kangnam style                             |
| 강남스타일                             | Oppa is Kangnam style, Kangnam style                             |
|                                        | Oppa is Kangnam style                                            |
| 오빤 강남스타일                        |                                                                  |
| 강남스타일                             | Eh- Sexy Lady, Oppa is Kangnam style                             |
|                                        | Eh- Sexy Lady oh oh oh oh                                        |
| 오빤 강남스타일                        |                                                                  |
|                                        | A girl who looks quiet but plays when she plays                  |
| Eh- Sexy Lady                          | A girl who puts her hair down when the right time comes          |
| 오빤 강남스타일                        | A girl who covers herself but is more sexy than a girl who bares |
|                                        | A sensable girl like that                                        |
| Eh- Sexy Lady                          |                                                                  |
| 에에에에에에                           | I’m a guy                                                       |
|                                        | A guy who seems calm but plays when he plays                     |
| 정숙해 보이지만 놀 땐 노는 여자        | A guy who goes completely crazy when the right time comes        |
| 이때다 싶으면 묶었던 머리 푸는 여자    | A guy who has bulging ideas rather than muscles                  |
| 가렸지만 웬만한 노출보다 야한 여자     | That kind of guy                                                 |
| 그런 감각적인 여자                     |                                                                  |
|                                        | Beautiful, loveable                                              |
| 나는 사나이                            | Yes you, hey, yes you, hey                                       |
| 점잖아 보이지만 놀 땐 노는 사나이      | Beautiful, loveable                                              |
| 때가 되면 완전 미쳐버리는 사나이       | Yes you, hey, yes you, hey                                       |
| 근육보다 사상이 울퉁불퉁한 사나이      | Now let’s go until the end                                      |
| 그런 사나이                            |                                                                  |
|                                        | Oppa is Kangnam style, Kangnam style                             |
| 아름다워 사랑스러워                    | Oppa is Kangnam style, Kangnam style                             |
| 그래 너 hey 그래 바로 너 hey           | Oppa is Kangnam style                                            |
|                                        |                                                                  |
| 아름다워 사랑스러워                    | Eh- Sexy Lady, Oppa is Kangnam style                             |
| 그래 너 hey 그래 바로 너 hey           | Eh- Sexy Lady oh oh oh oh                                        |
| 지금부터 갈 데까지 가볼까              |                                                                  |
|                                        | On top of the running man is the flying man, baby baby           |
| 오빤 강남스타일                        | I’m a man who knows a thing or two                              |
| 강남스타일                             | On top of the running man is the flying man, baby baby           |
|                                        | I’m a man who knows a thing or two                              |
| 오빤 강남스타일                        |                                                                  |
| 강남스타일                             | You know what I’m saying                                        |
|                                        | Oppa is Kangnam style                                            |
| 오빤 강남스타일                        |                                                                  |
|                                        | Eh- Sexy Lady, Oppa is Kangnam style                             |
| Eh- Sexy Lady                          | Eh- Sexy Lady oh oh oh oh                                        |
| 오빤 강남스타일                        | Oppa is Gangnam Style                                            |























































