What is langauge made up of? p68^^
EP3p68LanguageMadeOf.png^^
A small number of sounds put together in different ways. ^^
^]

How do people learn to use a language well? p69^^
EP3p69LearnLanguageWell.png^^
Through talking with others who use it well, reading good writers... ^^
^]

How necessary is language to our minds? p69^^
EP3p69LanguageNecessaryAir.png^^
As necessary as the air we breathe is to our bodies.^^
^]

What are lungs like? p71^^
EP3p71LungsLikeSponges.png^^
They are soft like sponges.^^
^]

What is blood? p72^^
EP3p72BloodIs.png^^
A red liquid which is in your body.^^
^]

What does our blood do for us? p72^^
EP3p72BloodDoesKa.png^^
It takes food to all parts of our bodies and waste away from those parts.^^
^]

When was the invention and development of the microscope? p73^^
EP3p73MicroscopeHstry.png^^
The microscope was invented and developed in the sixteenth and seventeenth centuries.^^
^]

What does the microscope let us do? p73^^
EP3p73MicroscopeLetsUs.png^^
The microscope lets us see cells.^^
^]

What are cells like? p74^^
EP3p74CellsLikeKa.png^^
Cells are like little flames. ^^
^]

What do fires burn? p74^^
EP3p74FiresBurnFuel.png^^
Fires burn their fuel. ^^
^]

What is the blood like? p75^^
EP3p75BloodLikeStream.png^^
Blood is like a stream. ^^
^]

What are cells like? p75^^
EP3p75CellsLikeRiverPlantsFishes.png^^
Cells are like plants and fishes in a river. ^^
^]

What number of our red blood cells die every second?  p75^^
EP3p75RedBloodCellsEverySecond.png^^
Three million of our red blood cells die every second.^^
^]

What sends the blood round the body in a stream? p76^^
EP3p76BloodSenderHeart.png^^
The heard sends the blood round the body. ^^
^]

What is the heart? p76^^
EP3p76LonelyHunter.png^^
The heart is a pump, and a lonely hunter.^^
^]

