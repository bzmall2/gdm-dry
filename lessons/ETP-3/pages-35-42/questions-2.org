#+TITLE: EP3 Questions 2
#+STARTUP: showall
#+OPTIONS: toc:nil num:nil timestamp:nil
#+HTML_HEAD: <style> @page { size: 210mm 297mm; margin: 4mm; margin-bottom: 6mm; } </style>
#+HTML_HEAD: <style> h1.title { display: none; }  </style>
#+HTML_HEAD: <style> h1, h2, h3, h4 { font-weight: 500; font-size: 1.2em; }  </style>
#+HTML_HEAD: <style> div#postamble { display: none; }  </style>
#+HTML_HEAD: <style> li { margin-bottom: 1.7em; }  </style>

** Questions for pages 35 ー 42

 1. What sort of plant is grass?
 2. What is the number of different sorts of grass?
 3. What sort of seeds does grass grow from?
 4. What do birds live on?
 5. What do seeds planted in good earth supply?
 6. What do seas and rivers supply?
 7. What did people have to eat before they made use of fire?
 8. What is bread made from?
 9. What is the chief food in Japan? (traditionally)
 10. What can be grown from a small amount seed planted in good earth?
 11. What is eaten more than wheat in eastern countries?
 12. If there were enough food in every country every day for every person, would the world be a better place?
 13. Why do people work?
     - Why do you work?
 14. Do we know one another's needs?
 15. Are the people of the earth hand in hand?
 16. How are people's minds put in touch with one another?
 17. How are we in touch with people everywhere?
 18. What do newspapers and television give us?
 19. What can be seen by the public everywhere?
 20. When did people start taking photographs?
    
    
