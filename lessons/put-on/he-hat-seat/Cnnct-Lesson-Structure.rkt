#lang slideshow

(require (file "../../5-scene-act-to-slides.rkt"))
(require (file "../../slide-utils.rkt"))

(define rect-info-lines (file->lines "coordinates-dimensions.txt"))
;; Attention Rectangle
(struct att-rct ([x #:auto] [y #:auto] [w #:auto]  [h  #:auto]  [fnt-num  #:auto]  [fndr  #:auto])
  #:mutable #:transparent
  #:auto-value 0)

(define-values (actor object place now was)
  (values (att-rct)(att-rct)(att-rct)(att-rct)(att-rct)))

;; t for strucT g for strinG
(define (set-struct-vals t g)
  (define l (string-split g))
  (set-att-rct-x! t (string->number (second l)))
  (set-att-rct-y! t (string->number (third l)))
  (set-att-rct-w! t (string->number (fourth l)))
  (set-att-rct-h! t (string->number (fifth l)))
  (set-att-rct-fnt-num! t (string->number (sixth l)))
  (set-att-rct-fndr! t  (seventh l)))

;; for finders get from string or symbol to procedure
;; https://stackoverflow.com/questions/29460841/dynamic-function-call-in-racket-or-get-a-procedure-from-a-string
(define ns (variable-reference->namespace (#%variable-reference)))
(define (string->procedure s)
  (unless (member s '("lt-find" "lc-find" "lb-find" "ct-find" "cc-find" "cb-find"))
    ;; add other finders later TODO!
    (raise-argument-error 'string->procedure "a permitted finder?" s))
  (define sym (string->symbol s))
  (eval sym ns))
;; (string->procedure "lt-find")
(define (get-fndr struct)
  (string->procedure (att-rct-fndr struct)))

(for-each (lambda (t g)
            (set-struct-vals t g))
         (list actor object place was now)
          rect-info-lines)

;;   x            y             w          h       f        n
;; (att-rct-x (att-rct-y (att-rct-w (att-rct-h (get-fndr (att-rct-fnt-num  
(define directive (att-rct))
(set-att-rct-x! directive (- (min (att-rct-x object) (att-rct-x place)) dir-rct-pad)); x 
(set-att-rct-y! directive (- (min (att-rct-y object) (att-rct-y place)) dir-rct-pad)) ; y
(set-att-rct-w! directive (get-dir-rct-len (att-rct-x object) (att-rct-w object)
					   (att-rct-x place) (att-rct-w place))) ; w
(set-att-rct-h! directive (get-dir-rct-len (att-rct-y object) (att-rct-h object)
 				    (att-rct-y place) (att-rct-h place))) ; h
(set-att-rct-fnt-num! directive 55); fnt-nun
(set-att-rct-fndr! directive lb-find) ; fndr
		   
(require rsvg)

(define img-ext ".svg")
(define img-ext-dir "svgs")
(define text-file "sentences.txt")
(define work-dir (current-directory-for-user))
;;(define img-dir (build-path work-dir img-ext-dir))
;; ; avoid absolute paths, copied directories won't work
(define text-file-path ; (build-path work-dir
				   text-file) ; )

(define all-image-files ;; paths for now
  (filter (lambda (p) (path-has-extension? p img-ext))
	  (directory-list img-ext-dir)))
;; (slide (para (map path->string all-image-files)))
;; gbases for img-bases??
(define bases-w-sentences-list 
  (map (lambda (s) (string-split s "\n"))
       (get-bases-and-sentences-string text-file-path)))
;; (slide (para (first bases-w-sentences-list))) ; ok!
(define img-bases
  (map (lambda (s) (first (string-split s)))
       (map first bases-w-sentences-list)))
(define (check-bases-w-img-dir)
  (define dir-bases (map path->string
			 (map (lambda (p) (path-replace-extension p ""))
			      all-image-files)))
  (for-each (lambda (base)
	      (unless (member base dir-bases)
		(raise-argument-error 'check-bases-w-img-dir
				      "a base-name in sentences.txt must correspond to an file in the image directory" base)))
	    img-bases))
(check-bases-w-img-dir)
;; (slide (para img-bases)) ok!
(define img-names-for-slides
  (map (lambda (b) (path-add-extension b img-ext))
       img-bases))
;; (slide (para (map path->string img-names-for-slides))) ;; ok!

(define w-r-for-sitpct-w-senpct .2) ;; width-ratio
(define (get-svg-pict path) ;; depends on (require rsvg)
  (svg-file->pict (build-path img-ext-dir path)))
(define all-sit-pcts ;; situation pictures
  (map scale-to-slide
       (map get-svg-pict img-names-for-slides)))
;; (slide (first situ-picts)) ; ok!
;; and from file module 5-scene-act-to-slides
;; (slide (scale-to-slide (first situ-picts))) ; ok! slightly bigger

;; settings
(define sentence-gap 2)
(define (a-sit-sens->t-pct a-situationz-sentences)
  (table 1 (map t-s a-situationz-sentences)
	 lt-superimpose lt-superimpose
	 sentence-gap sentence-gap))
;; TODO: keep base-name, or img-path with its sentence, maybe an a-list? 
(define all-sits-sentences-list
  (map cdr bases-w-sentences-list))
;; (slide (a-sit-sens->t-pct (first all-sits-sentences-list))) ; ok!

(define all-sits-sentences-pcts
  (map a-sit-sens->t-pct all-sits-sentences-list))
;; (slide (first all-sits-sentences-pcts)) ; ok!

(define sit-pct-gap 10)

(define (combine-sitpct-w-senpct sitpct senpct)
  (define sits-num (length all-sit-pcts))
  (define pct-h (/ client-h sits-num))
  (define scaled-sit (scale-to-fit sitpct
				   (* w-r-for-sitpct-w-senpct client-w)
				   pct-h)) ;; #:mode 'preserve))
  (define scaled-sen (scale-to-fit senpct
				   (* (- 1 w-r-for-sitpct-w-senpct) client-w)
				   pct-h));; #:mode 'preserve))
  (hc-append sit-pct-gap  scaled-sit scaled-sen))

(define (sit-n-sens-pics->slide sit-pct sen-pct)
  (slide (scale-to-slide (combine-sitpct-w-senpct sit-pct sen-pct))))

(define sits-pct-gap 10)
(define all-sits-pct
  (table 1 all-sit-pcts
	 lt-superimpose lt-superimpose
	 sits-pct-gap sits-pct-gap))
;; (slide (scale-to-slide all-sits-pct)) ok!
	 

(define all-sits-w-sens-pct
  (table 1 (map combine-sitpct-w-senpct all-sit-pcts all-sits-sentences-pcts)
	 lt-superimpose lt-superimpose
	 sits-pct-gap sits-pct-gap))
;; (slide all-sits-w-sens-pct)
;; End of 5-scene sequence with sentences
;; from gdm-act-sequence-pics-sens-to-slides-3.rkt
;;  TODO, mix in Start Scene

(define start-pict (first all-sit-pcts))
;; (slide start-pict) ; ok!
(define Start-scene-sens (first all-sits-sentences-list))
(define acr-sen (first Start-scene-sens))
(define obj-sen (second Start-scene-sens))
(define plc-sen (third Start-scene-sens))
(define dir-sen (fourth Start-scene-sens))
;; (slide (para (map t-s (list acr-sen obj-sen plc-sen dir-sen)))) ; ok!

(define end-pict (last all-sit-pcts))
;; (slide end-pict) ; ok!
(define End-scene-sens (last all-sits-sentences-list))
(define was-sen (first End-scene-sens))
(define now-sen (second End-scene-sens))
;; (slide (para (map t-s (list now-sen was-sen)))) ; ok!

;; Start-scene-picts
;;;; actor picts
(define acr-rct
   (make-attn-pct (att-rct-w actor) (att-rct-h actor) acr-clr))
(define acr-sen-pct ;; add angle later somehow??
   (make-attn-txt acr-sen (att-rct-fnt-num actor) acr-clr))
;;; object picts
(define obj-rct
  (make-attn-pct  (att-rct-w object) (att-rct-h object) obj-clr))
(define obj-sen-pct
  (make-attn-txt obj-sen (att-rct-fnt-num object) obj-clr))
;;;  place picts
(define plc-rct
  (make-attn-pct (att-rct-w place) (att-rct-h place) plc-clr))
(define plc-sen-pct
   (make-attn-txt plc-sen (att-rct-fnt-num place) plc-clr))
;;; directive picts
(define dir-rct
  (make-attn-pct (att-rct-w directive) (att-rct-h directive) dir-clr))
(define dir-sen-pct
  (make-attn-txt dir-sen (att-rct-fnt-num directive) dir-clr))
;;; End-scene picts
(define now-rct
  (make-attn-pct (att-rct-w now) (att-rct-h now) now-clr))
(define now-sen-pct
  (make-attn-txt now-sen (att-rct-fnt-num now) now-clr))
(define was-rct
  (make-attn-pct (att-rct-w was) (att-rct-h  was) was-clr))
(define was-sen-pct
  (make-attn-txt was-sen (att-rct-fnt-num was) was-clr))

(define acr-atn-pct
  (place-atn-rct (first all-sit-pcts) (att-rct-x actor) (att-rct-y actor) acr-rct))
;; (slide (scale-to-slide acr-atn-pct)) ; ok!
(define acr-sen-atn-pct ; (string->procedure (att-rct-fndr actor))
  (place-atn-sen acr-atn-pct acr-rct (get-fndr actor) acr-sen-pct))
;; (slide (scale-to-slide acr-sen-atn-pct)) ; ok!
(define obj-atn-pct
  (place-atn-rct acr-sen-atn-pct (att-rct-x object) (att-rct-y object) obj-rct))
;; (slide (scale-to-fit obj-atn-pct)) ; ok!
(define obj-sen-atn-pct
  (place-atn-sen obj-atn-pct obj-rct (get-fndr object) obj-sen-pct))
;; (slide (scale-to-slide obj-sen-atn-pct)) ; ok!
(define plc-atn-pct
  (place-atn-rct obj-sen-atn-pct (att-rct-x place) (att-rct-y place) plc-rct))
;; (slide (scale-to-slide plc-atn-pct)) ; ok!
(define plc-sen-atn-pct
  (place-atn-sen plc-atn-pct plc-rct (get-fndr place) plc-sen-pct))
;; (slide (scale-to-slide plc-sen-atn-pct)) ; ok!
(define dir-atn-pct
  (place-atn-rct plc-sen-atn-pct (att-rct-x directive) (att-rct-y directive) dir-rct))
;; (slide (scale-to-slide dir-atn-pct)) ; ok!
(define dir-sen-atn-pct
  (place-atn-sen dir-atn-pct dir-rct (att-rct-fndr directive) dir-sen-pct))
;; (slide (scale-to-slide dir-sen-atn-pct)) ; ok!
;; End-scene picts
(define now-atn-pct
  (place-atn-rct (last all-sit-pcts) (att-rct-x now) (att-rct-y now) now-rct))
;; (slide (scale-to-slide now-atn-pct)) ; ok!
(define now-sen-atn-pct 
  (place-atn-sen now-atn-pct now-rct (get-fndr now)  now-sen-pct))
;; ;; TODO figure out why panorama is needed here, find a better way
;; (slide (scale-to-slide (panorama now-sen-atn-pct))) ; ok!
(define was-atn-pct 
  (place-atn-rct now-sen-atn-pct (att-rct-x was) (att-rct-y was) was-rct))
;; (slide (scale-to-slide (panorama was-atn-pct))) ; ok!
(define was-sen-atn-pct
  (place-atn-sen was-atn-pct was-rct (get-fndr was) was-sen-pct))
;; (slide (scale-to-slide (panorama was-sen-atn-pct))) ; ok!

;; Tweak positions etc
;; (slide (scale-to-slide dir-sen-atn-pct))
;;  with addition of panorama to scale-to-slide in 5-scene-act-to-slides.rkt
;; ; the attention sentences will not run off the screen/page

;; Start Slide Sequence, ShowTime!
(slide (scale-to-slide all-sits-pct))

;; TODO (define (slide-s pct) (scale-to-slide (panorama pct))) ??
;; show first scene
(slide (scale-to-slide (first all-sit-pcts)))

(define (scale-all-to-slide pct)
  (scale-to-slide (panorama pct)))
;; Start scene rect and text lt-find lb-find
 (slide (scale-to-slide acr-atn-pct))
 (slide (scale-all-to-slide acr-sen-atn-pct))
 (slide (scale-all-to-slide obj-atn-pct))
 (slide (scale-all-to-slide obj-sen-atn-pct))
 (slide (scale-all-to-slide plc-atn-pct))
 (slide (scale-all-to-slide plc-sen-atn-pct))
 (slide (scale-all-to-slide dir-atn-pct))

 (slide (scale-all-to-slide dir-sen-atn-pct))

;; ; focus-in focus-out start-pict (first all-sit-pcts)??

;; show all scenes after Start Scene
;; (map slide (map scale-to-slide (rest all-sit-pcts)))

;; intersperes sit-sen-pics with sit-pics

(sit-n-sens-pics->slide (first all-sit-pcts)
			(first all-sits-sentences-pcts))

(slide (scale-to-slide (second all-sit-pcts)))
(sit-n-sens-pics->slide (second all-sit-pcts)
			(second all-sits-sentences-pcts))

(slide (scale-to-slide (third all-sit-pcts)))
(sit-n-sens-pics->slide (third all-sit-pcts)
			(third all-sits-sentences-pcts))

(slide (scale-to-slide (fourth all-sit-pcts)))
(sit-n-sens-pics->slide (fourth all-sit-pcts)
			(fourth all-sits-sentences-pcts))

(slide (scale-to-slide (last all-sit-pcts)))

;; End Scene attn rects
(slide (scale-to-slide now-atn-pct))
(slide (scale-to-slide (panorama now-sen-atn-pct)))

(slide (scale-to-slide (panorama was-atn-pct)))
(slide (scale-to-slide (panorama was-sen-atn-pct)))

(sit-n-sens-pics->slide (fifth all-sit-pcts)
			(fifth all-sits-sentences-pcts))
;; focus-in focus-out
;; ;; record zoom to see how tux-type and slideshow F11 work..
;; ;; ;; once you get focus-in and focus-out working

;; individual pic-n-sentences
(for-each sit-n-sens-pics->slide all-sit-pcts all-sits-sentences-pcts)

(slide (scale-to-slide all-sits-w-sens-pct))

(slide (scale-to-slide all-sits-pct))
