#+TITLE: Cement Production and Area by country
#+STARTUP: showall
#+OPTIONS: toc:nil num:nil timestamp:nil
#+HTML_HEAD: <style> @page { size: 210mm 297mm; margin: 4mm; margin-bottom: 6mm; } </style>
#+HTML_HEAD: <style> h1.title { display: none; }  </style>
#+HTML_HEAD: <style> h1, h2, h3, h4 { font-weight: 500; font-size: 1.2em; }  </style>
#+HTML_HEAD: <style> div#postamble { display: none; }  </style>



** Quote
"In 1994 concrete production in Japan totaled 91.6 million tons, compared with 77.9 million tons in the United States. This means that Japan lays about thirty times as much per square foot as the United States"
  _Toxic Archipelago_ Brett Walker

*** 1994 figures
| USA |  77.9 | 
| Japan | 91.6 | 

** More Recent Figures: start
*** Area in 2021, km^2
| Japan        |   364500.0 |
| USA          |  9147420.0 |   

 -  https://data.worldbank.org/indicator/AG.LND.TOTL.K2
*** Cement Production 2014 million metric tons

| Country | 2014 | 2013 |
| Japan   |   58 |   57 |
| USA     |   83 |   77 |

 - https://en.wikipedia.org/wiki/Cement_production_by_country
***


** Wikipedia

 2022年  6月 19日 日曜日 15:09:37 JST

   https://en.wikipedia.org/wiki/Geography_of_Japan

The territory covers 377,973.89 km2 (145,936.53 sq mi). It is the fourth largest island country in the world and the largest island country in

2021 km^2
| USA   | 9147420.0  |
| Japan | 364500.0  |

# 377973.89 |


** Land Area Figures

   https://data.worldbank.org/indicator/AG.LND.TOTL.K2

*** 2021   
| China        |            |
| Cuba         |  9424702.9 |
| Germany      |   349380.0 |
| India        |  2973190.0 |
| Indonesia    |  1877519.0 |
| Japan        |   364500.0 |
| Korea        |    97520.0 |
| N. Korea     |   120410.0 |
| Malaysia     |   328550.0 |
| Mexico       |  1943950.0 |
| Phillipines  |   298170.0 |
| Russia       | 16376870.0 |
| Saudi Arabia |  2149690.0 |
| USA          |  9147420.0 |
| Vietnam      |   310070.0 |

** Concrete Figures

https://en.wikipedia.org/wiki/Cement_production_by_country

    
Batshit Constructions in China? It's not as bad as nuclears submarines, bombs, and F37s but still bad for the environment.


[[file:imgs/Statistica-Data-Concrete-Production-20220619-scrnshot.png]]

*** Hydraulic Cement

| Country         |  2014 |  2013 |  2012 |  2011 |  2010 |  2009 |  2008 |  2007 |  2006 |  2005 |  2004 |  2003 |  2002 |  2001 |  2000 |
| Brazil          |    72 |    70 |    69 |    64 |    59 |    52 |    52 |    46 |    40 |    37 |    38 |    40 |    38 |    40 |    39 |
| China PRC       | 2,500 | 2,420 | 2,210 | 2,100 | 1,880 | 1,630 | 1,390 | 1,350 | 1,200 | 1,040 |   934 |   813 |   705 |   627 |   583 |
| Egypt           |    50 |    50 |    46 |    44 |    48 |    47 |    40 |    38 |    29 |    29 |    28 |    29 |    23 |    25 |    24 |
| France          |       |       |       |       |       |       |    21 |    22 |    21 |    21 |    21 |    20 |    20 |    20 |    20 |
| Germany         |    31 |    31 |    32 |    34 |    30 |    30 |    34 |    33 |    33 |    31 |    32 |    30 |    30 |    28 |    38 |
| India           |   280 |   280 |   270 |   240 |   210 |   205 |   177 |   170 |   155 |   145 |   125 |   110 |   100 |   100 |    95 |
| Indonesia       |    60 |    56 |    32 |    30 |    22 |    40 |    37 |    36 |    34 |    37 |    36 |    35 |    33 |    31 |    28 |
| Iran            |    75 |    72 |    70 |    61 |    50 |    50 |    44 |    36 |    33 |    33 |    30 |    30 |    30 |    27 |    20 |
| Italy           |    22 |    22 |    33 |    33 |    36 |    36 |    43 |    48 |    43 |    46 |    38 |    38 |    40 |    40 |    36 |
| Japan           |    58 |    57 |    51 |    51 |    52 |    55 |    63 |    68 |    70 |    70 |    67 |    71 |    72 |    77 |    81 |
| Korea ROK       |    48 |    47 |    48 |    48 |    47 |    50 |    54 |    57 |    55 |    51 |    54 |    59 |    56 |    52 |    51 |
| Mexico          |    35 |    35 |    35 |    35 |    35 |    35 |    48 |    41 |    41 |    36 |    35 |    32 |    31 |    30 |    32 |
| Pakistan        |    32 |    31 |    32 |    32 |    30 |    32 |    39 |    26 |       |       |       |       |       |       |       |
| Russia          |    69 |    66 |    62 |    56 |    50 |    44 |    54 |    60 |    55 |    49 |    43 |    41 |    38 |    35 |    32 |
| Saudi Arabia    |    63 |    57 |    50 |    48 |    42 |    40 |    32 |    30 |    27 |    26 |    23 |    23 |    21 |    21 |    21 |
| Spain           |       |       |       |    22 |    24 |    50 |    42 |    54 |    54 |    50 |    47 |    42 |    43 |    41 |    30 |
| Taiwan          |       |       |       |       |       |       |       |       |       |       |       |       |       |       |    19 |
| Thailand        |    42 |    42 |    37 |    37 |    37 |    31 |    36 |    36 |    39 |    38 |    36 |    33 |    32 |    28 |    32 |
| Turkey          |    75 |    71 |    64 |    63 |    63 |    54 |    51 |    50 |    48 |    43 |    38 |    33 |    33 |    30 |    36 |
| United States A |    83 |    77 |    75 |    69 |    67 |    65 |    88 |    97 |   100 |   101 |    99 |    94 |    91 |    91 |    90 |
| Vietnam         |    60 |    58 |    60 |    59 |    50 |    48 |    36 |    36 |    32 |    29 |    25 |       |       |       |       |
| Rest of world   |   525 |   536 |   524 |   470 |   480 |   466 |   459 |   437 |   442 |   400 |   381 |   380 |   360 |   361 |   330 |
| Whole World     | 4,180 | 4,080 | 3,800 | 3,600 | 3,310 | 3,060 | 2,840 | 2,770 | 2,550 | 2,310 | 2,130 | 1,950 | 1,800 | 1,700 | 1,600 |

*** Japan by year

   Japan:  https://www.indexmundi.com/minerals/?country=jp&product=cement&graph=production

    USA:
https://www.indexmundi.com/minerals/?country=us&product=cement&graph=production

** Cement and CO2
https://www.iea.org/reports/cement

The direct CO2 intensity of cement production increased 1.8% per year during 2015-2020. In contrast, 3% annual declines to 2030 are necessary to get on track with the Net Zero Emissions by 2050 Scenario. Sharper focus is needed in two key areas: reducing the clinker-to-cement ratio (including through greater uptake of blended cements) and deploying innovative technologies (including CCUS). Governments can stimulate investment and innovation in these areas by funding R&D and demonstration, and adopting mandatory CO2 emissions reduction policies.

Reducing CO2 emissions while producing enough cement to meet demand will be challenging, especially since demand growth is expected to resume as the slowdown in Chinese activity is offset by expansion in other markets. 

Key strategies to cut carbon emissions in cement production include improving energy efficiency, switching to lower-carbon fuels, promoting material efficiency (to reduce the clinker-to-cement ratio and total demand) and advancing process and technology innovations such as CCS. The latter two contribute the most to direct emissions reductions in the Net Zero Emissions by 2050 Scenario. 

Demand for cement in the construction industry drives production and is thus an important determinant of cement subsector energy consumption and CO2 emissions. Initial estimates suggest that 4.3 Gt of cement were produced globally in 2020. This is a modest increase from 4.2 Gt the previous year, driven in large part by infrastructure-related stimulus projects in China. China is the largest cement producer, accounting for about 55% of global production, followed by India at 8%.

In the Net Zero Emissions by 2050 Scenario, global cement production stays relatively flat to 2030. Production is likely to decline in China in the long term, but increases are anticipated in India, other developing Asian countries and Africa as these regions develop their infrastructure.  

Adopting material efficiency strategies to optimise the use of cement can help reduce demand along the entire construction value chain, helping to cut CO2 emissions from cement production. Therefore, demand in 2030 in the Net Zero Emissions by 2050 Scenario is 6% lower than in a baseline scenario in which no steps are taken to reduce demand. 

Actions to reduce cement demand include optimising the use of cement in concrete mixes, using concrete more efficiently, minimising waste in construction, and maximising the design life of buildings and infrastructure. Material efficiency efforts have gained increasing support in recent years. 

Globally, the energy intensities of thermal energy and electricity have continued to decline gradually as dry-process kilns – including staged preheaters and precalciners (considered state-of-the-art technology) – replace wet-process kilns, and as more efficient grinding equipment is deployed. 

The global thermal energy intensity of clinker is estimated to have remained relatively flat over the past five years, at 3.4-3.5 GJ/t. Although it has declined in some regions, a slight increase in energy intensity in China has led to an overall global increase. Fossil fuels continue to provide the majority of energy in the cement sector, with bioenergy and biomass-based wastes accounting for only 3% of thermal energy used in 2020. 

In the Net Zero Emissions by 2050 Scenario, the thermal energy intensity of clinker production declines 0.8% per year to a global average of 3.2 GJ/t, and the electricity intensity of cement production falls by 1.9% per year to 84 kWh/t. This excludes additional energy required for emissions reductions technologies such as CCUS. 

The share of bioenergy and renewable waste grows considerably to 15% in 2030 in the Net Zero Emissions by 2050 Scenario. Meanwhile, the share of fossil-based waste (such as tyres, waste oil and plastics) remains at ~5% of fuel use.
2022年  6月 19日 日曜日 15:40:04 JST

