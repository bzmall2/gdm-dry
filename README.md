# gdm-dry

Develop scripts for gdm teachers to use in DrRacket. The teacher will put directory names in the scripts. The scripts will  put out a page for printing on paper and slides for showing on screen. 

## Aim

My aim is to make it simple for veteran GDM[^GDM] teachers to make use of the Racket language for teaching. These short programs may also work as example for the D.R.Y. principle: **D**on't **R**epeat **Y**ourself.

I make use of the same sentences and pictures to make both  one-page worksheets for printing on paper and multi-page slideshows for showing on-screen. My use of racket programs is from the command-line using a Free Softwars Operating System. But a great number of veteran GDM teachers may not use a command-line **shell** (like *bash*) so I hope to adapt the scripts for use in DrRacket. My idea is that teachers with limited computer experience may be able to make use of DrRacket and certain conventions.

This attempt will give me a chance to think through the use of not only the *DRY* principle but also the idea of *Convention over Configuration*(**CoC**). My hope is to discover formats and uses that will take care of the little details and let us put our attention on the making of good teaching materials.

When letting learners see slideshows, or when making presentations, it is not necessary to use heavy applications of software. We are able to use writing just as we are learning and teaching with the EP books and GDM. DrRacket[^DrR] langauge gives a chance to do good GDM teaching with a computer language[^qckpct] as seen in the book *How to Design Programs*.[^HTDP]

> This book is the first book on programming as the core subject of a liberal arts education. Its main focus is the design process that leads from problem statements to well-organized solutions; it deemphasizes the study of programming language details, algorithmic minutiae, and specific application domains. Our desire to focus on the design process requires two radical innovations for introductory courses. The first innovation is a set of explicit design guidelines. Existing curricula tend to provide vague and ill-defined suggestions, such as "design from top to bottom" or "make the program structural." We have instead developed design guidelines that lead students from a problem statement to a computational solution in step-by-step fashion with well-defined intermediate products.

The script code and learning sequences I hope to share like Free Softward Documentation. However these first examples will make use of drawings by Mio Sato who will retain all the copyrights to the photos. If the scripts come to be used by teachers and learners the aim will be that each class, and each learner can generate their own pages and slideshows. Our aim could be that everyone grows their own textbook. 

[^GDM]: https://www.gdm-japan.net/
[^DrR]: https://docs.racket-lang.org/drracket/interface-essentials.html
[^qckpct]: https://docs.racket-lang.org/quick/index.html
[^HTDP]: https://htdp.org/2003-09-26/Book/curriculum-Z-H-2.html

